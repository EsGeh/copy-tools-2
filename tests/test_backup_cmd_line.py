from copy_tools import _utils as utils
from copy_tools import _constants as constants

from copy_tools.commands.ct_backup import parse_args

from pathlib import Path
import pytest



default_parse_res = dict(
        exclude=[],
        config_dir=None,
        init_config=False,
        dry_run=False,
        print_opts=False,
        log_user=None,
        log_group=None,
)
minimal_argv = [ "src", "dst" ]
minimal_parse_res = {
        **default_parse_res,
        **dict(
            src=Path("src"),
            dst=Path("dst"),
        ),
}

def test_cmd_line_frontend():
    for argv, expected_res in [
            (
                minimal_argv,
                minimal_parse_res,
            ),
            # --exclude=:
            (
                minimal_argv + [ "--exclude", "excluded" ],
                { **minimal_parse_res, "exclude" : ["excluded"] },
            ),
            (
                minimal_argv + [ "--exclude", "excluded", "--exclude", "other_excluded"],
                { **minimal_parse_res, "exclude" : ["excluded", "other_excluded"] },
            ),
            # --config-dir=:
            (
                minimal_argv + [ "--config-dir", "config" ],
                { **minimal_parse_res, "config_dir" : Path("config") },
            ),
            # --init-config:
            (
                minimal_argv + [ "--init-config" ],
                { **minimal_parse_res, "init_config" : True },
            ),
            # -p|--print-opts:
            (
                minimal_argv + [ "--print-opts" ],
                { **minimal_parse_res, "print_opts" : True },
            ),
            (
                minimal_argv + [ "-p" ],
                { **minimal_parse_res, "print_opts" : True },
            ),
            # -n|--dry-run:
            (
                minimal_argv + [ "--dry-run" ],
                { **minimal_parse_res, "dry_run" : True },
            ),
            (
                minimal_argv + [ "-n" ],
                { **minimal_parse_res, "dry_run" : True },
            ),
            # -u|--log-user:
            (
                minimal_argv + [ "--log-user", "testuser" ],
                { **minimal_parse_res, "log_user" : "testuser" },
            ),
            (
                minimal_argv + [ "-u", "testuser" ],
                { **minimal_parse_res, "log_user" : "testuser" },
            ),
            # -g|--log-group:
            (
                minimal_argv + [ "--log-group", "testgroup" ],
                { **minimal_parse_res, "log_group" : "testgroup" },
            ),
            (
                minimal_argv + [ "-g", "testgroup" ],
                { **minimal_parse_res, "log_group" : "testgroup" },
            ),
    ]:
        cmd_args = parse_args( argv )
        assert cmd_args == expected_res

def test_too_few_params():
    argv = minimal_argv[:-1]
    with pytest.raises( SystemExit ):
        cmd_args = parse_args( argv )

def test_too_many_params():
    argv = minimal_argv + [ "other arg" ]
    with pytest.raises( SystemExit ):
        cmd_args = parse_args( argv )
