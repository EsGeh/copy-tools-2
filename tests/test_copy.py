from copy_tools import _utils as utils
from tests import utils as test_utils

import pytest
import filecmp


@pytest.fixture
def copy_params(tmp_path):
    src = tmp_path / "src"
    dst = tmp_path / "dst"
    test_utils.create_file_tree( src )
    dst.mkdir()
    return dict(
            src = src,
            dst = dst,
            dry_run = False,
    )

def test_copy(
        copy_params
):
    utils.copy(
            **copy_params
    )
    comparison = filecmp.dircmp(
            copy_params['src'],
            copy_params['dst'],
    )
    assert comparison.left_list == comparison.right_list
