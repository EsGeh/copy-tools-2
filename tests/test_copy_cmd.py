from copy_tools.commands import ct_copy
from copy_tools import _constants as constants
from tests.utils import (
        copy_params,
)
from tests import utils as test_utils

from pathlib import Path
import pytest
import os
import time


command = ct_copy

@pytest.fixture
def dry_run_params(tmp_path):
    tmp_path = tmp_path / "dry_run"
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = True,
            log_file = tmp_path / "config" / "log",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

@pytest.fixture
def changed_dir_params(tmp_path):
    tmp_path = tmp_path / "normal_run"
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = False,
            log_file = tmp_path / "config" / "log",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

def test_run(
        copy_params,
        capsys,
):
    params = copy_params
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_run_with_relative_paths(
        tmp_path,
        copy_params,
        capsys,
):
    params = copy_params
    create_test_files( params )
    os.chdir( tmp_path )
    params['src'] = params['src'].relative_to(tmp_path)
    params['dst'] = params['dst'].relative_to(tmp_path)
    params['log_file'] = params['log_file'].relative_to(tmp_path)
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_dry_run(
        dry_run_params,
        changed_dir_params,
        capsys,
):
    # dry run:
    create_test_files( dry_run_params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **dry_run_params,
    )
    assert ret == 0
    # dry run copies nothing:
    dst_files = test_utils.dir_to_file_list(
            dry_run_params['dst'],
    )
    assert dst_files == []
    assert not dry_run_params['log_file'].exists()
    dry_run_captured=capsys.readouterr()
    # normal run:
    create_test_files( changed_dir_params )
    ret = command.run(
            now=now,
            **changed_dir_params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    # dry run stdout is same as if run normally:
    dry_run_out = \
            dry_run_captured.out \
            .replace(
                    str(dry_run_params["src"]),
                    str(changed_dir_params["src"]),
            ) \
            .replace(
                    str(dry_run_params["dst"]),
                    str(changed_dir_params["dst"]),
            ) \
            .replace(
                    str(dry_run_params["log_file"]),
                    str(changed_dir_params["log_file"]),
            ) \
            .replace(
                    " --dry-run",
                    "",
            )
    assert dry_run_out == captured.out

def test_fail_if_src_not_existing(
        copy_params,
        capsys,
):
    params = copy_params
    create_test_files(
            params,
            dont_create_src=True,
    )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret != 0
    assert list(params['dst'].iterdir()) == []
    captured=capsys.readouterr()
    check_cmd_fails(
            params,
            captured,
    )

def test_dont_create_subdirs_on_dst(
        copy_params,
        capsys,
):
    params = copy_params
    params['dst'] = params['dst'] / "subdir"
    create_test_files(
            params,
            dont_create_dst=True,
    )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret != 0
    assert not params['dst'].exists()
    captured=capsys.readouterr()
    check_cmd_fails(
            params,
            captured,
    )

def test_print_opts(
        copy_params,
        capsys,
):
    params = copy_params
    params['print_opts'] = True
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_deletes_files_on_src(
        copy_params,
        capsys,
):
    params = copy_params
    config_dir = params['log_file'].parent
    src = params['src']
    dst = params['dst']
    config_dir.mkdir()
    src.mkdir()
    (src / "a").touch()
    (src / "c").mkdir()
    (src / "c/c1").touch()
    test_utils.create_file_tree( dst )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

# semantics are borrowed from rsync:
# paths starting with '/' are
# considered to be relative
# to the src.
# paths not starting with '/'
# are patterns that match everywhere
# in a path

def test_copy_exclude_rel_pattern(
        tmp_path,
        copy_params,
        capsys,
):
    params = copy_params
    params['exclude'] = [
            "a",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_copy_exclude_abs_pattern(
        tmp_path,
        copy_params,
        capsys,
):
    params = copy_params
    params['exclude'] = [
            "/a",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_copy_exclude_dir(
        tmp_path,
        copy_params,
        capsys,
):
    params = copy_params
    params['exclude'] = [
            "c",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

#######################
# utils:
#######################

def create_test_files(
        params,
        dont_create_src=False,
        dont_create_dst=False,
        dont_create_log_dir=False,
):
    dont_create_src or test_utils.create_file_tree(
            params['src'],
    )
    dont_create_dst or params['dst'].mkdir(parents=True)
    dont_create_log_dir or params['log_file'].parent.mkdir(parents=True)

def check_cmd_fails(
        params,
        captured,
):
    output_lines = captured.err.split( "\n" )
    contains_err_msg = list(filter(
            lambda x: test_utils.matches_format_str( x, constants.ERROR_MSG ),
            output_lines
    ))
    assert len(contains_err_msg) > 0
    log_file_path = params['log_file']
    if (
            log_file_path.exists()
    ):
        log_content = log_file_path.read_text()
        log_lines = log_content.split( "\n" )
        test_utils.assert_output_lines_contain_format_str(
                constants.ERROR_MSG,
                log_lines,
        )

def check_cmd_succeeds(
        params,
        captured,
):
    check_dst( params )
    test_utils.check_cmd_output(params, captured)
    check_logs(
            params,
            captured,
    )

def check_logs(
        params,
        captured,
):
    assert params['log_file'].exists()
    assert not params['log_file'].is_dir()
    output_lines = captured.out.split("\n")
    # log file should be exactly the same
    # except the first line which reports creating the log file:
    log_file_lines = params['log_file'].read_text().split("\n")
    output_lines = [
            line for line in output_lines
            if line != constants.CREATE_FILE_MSG.format(path=params['log_file'].resolve() )
    ]
    assert output_lines == log_file_lines

def check_dst(
        params
):
    dst_files, src_files = test_utils.check_dirs_equal(
            params['src'],
            params['dst'],
            params.get('exclude'),
    )
    assert dst_files == src_files

    # src_files = dir_to_file_list( params['src'] )
    # dst_files = dir_to_file_list( params['dst'] )
    # excluded = []
    # for pattern in params.get('exclude', []):
    #     if pattern.startswith( "/" ):
    #         path = Path(pattern).relative_to( "/" )
    #         excluded.append( path )
    #     else:
    #         for path in src_files:
    #             if pattern in str(path):
    #                 excluded.append( path )
    # for excl in excluded:
    #     src_files.remove( excl )
    # assert dst_files == src_files
