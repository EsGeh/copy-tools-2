from copy_tools.commands import ct_backup
from copy_tools import _constants as constants
from tests.utils import (
        backup_params,
)
from tests import utils as test_utils

from pathlib import Path
import pytest
import os
import time


command = ct_backup

@pytest.fixture
def dry_run_params(tmp_path):
    tmp_path = tmp_path / "dry_run"
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = True,
            config_dir = tmp_path / "config",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

@pytest.fixture
def changed_dir_params(tmp_path):
    tmp_path = tmp_path / "normal_run"
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = False,
            config_dir = tmp_path / "config",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

def test_run(
        backup_params,
        capsys,
):
    params = backup_params
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_run_with_relative_paths(
        tmp_path,
        backup_params,
        capsys,
):
    params = backup_params
    create_test_files( params )
    os.chdir( tmp_path )
    params['src'] = params['src'].relative_to(tmp_path)
    params['dst'] = params['dst'].relative_to(tmp_path)
    params['config_dir'] = params['config_dir'].relative_to(tmp_path)
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_dry_run(
        dry_run_params,
        changed_dir_params,
        capsys,
):
    # dry run:
    create_test_files(
            dry_run_params,
            # dont_create_cfg=True,
    )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **dry_run_params,
    )
    assert ret == 0
    # dry run copies nothing:
    dst_files = test_utils.dir_to_file_list(
            dry_run_params['dst'],
    )
    assert dst_files == []
    assert list(dry_run_params['config_dir'].iterdir()) == [dry_run_params['config_dir'] / "log"]
    assert list((dry_run_params['config_dir'] / "log").iterdir()) == []
    dry_run_captured=capsys.readouterr()
    create_test_files(
            changed_dir_params,
            # dont_create_cfg=True,
    )
    ret = command.run(
            now=now,
            **changed_dir_params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    # dry run stdout is same as if run normally:
    dry_run_out = \
            dry_run_captured.out \
            .replace(
                    str(dry_run_params["src"]),
                    str(changed_dir_params["src"]),
            ) \
            .replace(
                    str(dry_run_params["dst"]),
                    str(changed_dir_params["dst"]),
            ) \
            .replace(
                    str(dry_run_params["config_dir"]),
                    str(changed_dir_params["config_dir"]),
            ) \
            .replace(
                    " --dry-run",
                    "",
            )
    assert dry_run_out == captured.out

def test_fail_if_src_not_existing(
        backup_params,
        capsys,
):
    params = backup_params
    create_test_files(
            params,
            dont_create_src=True,
    )
    now = time.gmtime()
    ret = command.run(
            now,
            **params,
    )
    assert ret != 0
    assert list(params['dst'].iterdir()) == []
    captured=capsys.readouterr()
    check_cmd_fails(
            params,
            captured,
    )

# REMARK:
# backup command recursively creates
# necessary dirs on dst

def test_fail_for_unknown_reason(
        backup_params,
        capsys,
):
    params = backup_params
    create_test_files(
            params,
    )
    now = time.gmtime()
    _ret = command.run(
            now=now,
            **params,
            emulate_interrupt=True,
    )
    captured=capsys.readouterr()
    check_interrupted(
            params,
            captured,
    )

def test_print_opts(
        backup_params,
        capsys,
):
    params = backup_params
    params['print_opts'] = True
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_deletes_files_on_src(
        backup_params,
        capsys,
):
    params = backup_params
    config_dir = params['config_dir']
    src = params['src']
    dst = params['dst']
    config_dir.mkdir()
    src.mkdir()
    (src / "a").touch()
    (src / "c").mkdir()
    (src / "c/c1").touch()
    (params['config_dir'] / "log").mkdir()
    test_utils.create_file_tree( dst )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

# semantics are borrowed from rsync:
# paths starting with '/' are
# considered to be relative
# to the src.
# paths not starting with '/'
# are patterns that match everywhere
# in a path

def test_copy_exclude_rel_pattern(
        tmp_path,
        backup_params,
        capsys,
):
    params = backup_params
    params['exclude'] = [
            "a",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_copy_exclude_abs_pattern(
        tmp_path,
        backup_params,
        capsys,
):
    params = backup_params
    params['exclude'] = [
            "/a",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

def test_copy_exclude_dir(
        tmp_path,
        backup_params,
        capsys,
):
    params = backup_params
    params['exclude'] = [
            "c",
    ]
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    check_cmd_succeeds(
            params,
            captured,
    )

## auto excludes:

def test_config_in_src(
        tmp_path,
        backup_params,
        capsys,
):
    """If config is inside SRC,
    it will be excluded from rsync
    and copied seperately, after the
    log has been closed.
    This avoids changes being reported
    in the log file during the next backup.

    TODO: more detailed asserts
    """
    params = backup_params
    params['config_dir'] = params['src'] / "config"
    create_test_files( params )
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    backups = check_dst_backups( params )
    check_dst_logs(
            params,
            backups,
    )
    test_utils.check_cmd_output(params, captured)

# definitely not an exhaustive test
# e.g. logging after continuing
# is not tested to be the same as
# stdout. This is just assumed.
def test_creates_running_marker(
        backup_params,
        capsys,
):
    params = backup_params
    create_test_files( params )
    now = time.gmtime()
    _ret = command.run(
            now=now,
            **params,
            emulate_interrupt = True,
    )
    time.sleep( 1.1 )
    # ret is unspecified
    # flush stdout / stderr:
    captured = capsys.readouterr()
    check_interrupted(
            params,
            captured,
    )
    # when running again - the backup should be continued:
    # - the last log file shall be reused
    # - the same dst dir will be used
    now = time.gmtime()
    ret = command.run(
            now=now,
            **params,
    )
    assert ret == 0
    captured=capsys.readouterr()
    backups = check_dst_backups( backup_params )
    assert len(backups) == 1
    check_dst_logs(
            backup_params,
            backups,
    )
    test_utils.check_cmd_output(backup_params, captured)

#######################
# utils:
#######################

def create_test_files(
        params,
        dont_create_src=False,
        dont_create_dst=False,
        dont_create_cfg=False,
):
    dont_create_src or test_utils.create_file_tree( params['src'] )
    dont_create_dst or params['dst'].mkdir()
    dont_create_cfg or params['config_dir'].mkdir()
    dont_create_cfg or (params['config_dir'] / "log").mkdir()

def check_cmd_fails(
        params,
        captured,
):
    output_lines = captured.err.split( "\n" )
    contains_err_msg = list(filter(
            lambda x: test_utils.matches_format_str( x, constants.ERROR_MSG ),
            output_lines
    ))
    assert len(contains_err_msg) > 0
    if (
            params['config_dir'].is_dir()
            and (params['config_dir'] / 'log').is_dir()
    ):
        log_files = list((params['config_dir'] / "log").iterdir())
        assert len(log_files) == 2
        log_files.remove( params['config_dir'] / "log" / "current.log" )
        log_file_path = log_files[0]
        log_content = log_file_path.read_text()
        log_lines = log_content.split( "\n" )
        test_utils.assert_output_lines_contain_format_str(
                constants.ERROR_MSG,
                log_lines,
        )

def check_cmd_succeeds(
        backup_params,
        captured,
):
    check_local_logs(
            backup_params,
            captured,
    )
    backups = check_dst_backups( backup_params )
    check_dst_logs(
            backup_params,
            backups,
    )
    test_utils.check_cmd_output(backup_params, captured)

def check_dst_backups(
        backup_params,
):
    # backup dir:
    dst_backup_dir = backup_params['dst'] / 'backup'
    assert dst_backup_dir.exists()
    assert dst_backup_dir.is_dir()
    # link to the last backup:
    last_backup_link = backup_params['dst'] / 'last'
    assert last_backup_link.is_symlink()
    assert not last_backup_link.readlink().is_absolute()
    link_dest = last_backup_link.parent / last_backup_link.readlink()
    assert link_dest.exists()
    assert link_dest.is_dir()
    # backups contains >0 files
    # and last_backup_link points to one of them:
    backups = list(dst_backup_dir.iterdir())
    assert len(backups) > 0
    assert link_dest in backups
    last_backup = link_dest
    # last backup contains the copied files:
    dst_files, src_files = test_utils.check_dirs_equal(
            backup_params['src'],
            last_backup,
            backup_params.get('exclude'),
    )
    assert dst_files == src_files
    return backups

def check_local_logs(
        backup_params,
        captured,
):
    assert backup_params['config_dir'].is_dir()
    assert (backup_params['config_dir'] / 'log').is_dir()
    log_files = list((backup_params['config_dir'] / 'log').iterdir())
    assert len(log_files) == 1
    log_file_path = log_files[0]
    assert not log_file_path.is_dir()
    output_lines = captured.out.split("\n")
    # log file should be exactly the same
    # except the first line which reports creating the log file:
    log_file_lines = log_file_path.read_text().split("\n")
    output_lines = [
            line for line in output_lines
            if line not in [
                constants.CREATE_FILE_MSG.format(path=backup_params['config_dir'].resolve() ),
                constants.CREATE_FILE_MSG.format(path=backup_params['config_dir'].resolve() / "log" ),
                constants.CREATE_FILE_MSG.format(path=log_file_path.resolve() ),
            ]
    ]
    assert output_lines == log_file_lines

def check_dst_logs(
        backup_params,
        backups,
):
    dst_log_dir = backup_params['dst'] / 'log'
    assert dst_log_dir.exists()
    assert dst_log_dir.is_dir()
    # for every backup, there is a log:
    log_files = list(dst_log_dir.iterdir())
    for backup in backups:
        log_file = f"{backup.name}.log"
        assert log_file in map(lambda f: f.name, log_files)
        log_files = [f for f in log_files if f.name != log_file]
    assert log_files == []

def check_interrupted(
        params,
        captured,
):
    check_local_logs_interrupted(
            params,
            captured,
    )

def check_local_logs_interrupted(
        params,
        captured,
):
    output_lines = captured.out.split("\n")
    log_files = list((params['config_dir'] / 'log').iterdir())
    assert len(log_files) == 2
    log_dir = params['config_dir'] / 'log'
    marker_link = log_dir / 'current.log'
    normal_log_paths = list(filter(
        lambda f: f !=  marker_link,
        log_files,
    ))
    assert len(normal_log_paths) == 1
    normal_log_path = normal_log_paths[0]
    assert marker_link.exists()
    assert marker_link.is_symlink()
    assert marker_link.readlink() == normal_log_path.relative_to( log_dir )
    # log file should be exactly the same
    # except the first line which reports creating the log file:
    log_file_lines = normal_log_path.read_text().split("\n")
    output_lines = [
            line for line in output_lines
            if line not in [
                constants.CREATE_FILE_MSG.format(path=params['config_dir'].resolve() ),
                constants.CREATE_FILE_MSG.format(path=params['config_dir'].resolve() / "log" ),
                constants.CREATE_FILE_MSG.format(path=normal_log_path.resolve() ),
            ]
    ]
    assert output_lines == log_file_lines
