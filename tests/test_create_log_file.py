from copy_tools import _utils as utils
from copy_tools import _constants as constants

from pathlib import Path
import pytest


@pytest.fixture
def log_file_params(tmp_path):
    return dict(
            file_path = tmp_path / 'test.log',
            dry_run = False,
            user = None,
            group = None,
    )

@pytest.fixture
def log_dir_params(tmp_path):
    return dict(
            log_dir = tmp_path,
            create_dirs = False,
            dry_run = False,
            user = None,
            group = None,
            print_func = print,
    )

def test_create_file(
        log_file_params,
        capsys,
):
    params = log_file_params
    try:
        log_file = utils.create_file(
                **params,
        )
        assert log_file is not None
        assert Path(log_file.name) == log_file_params['file_path']
        captured=capsys.readouterr()
        assert captured.out == constants.CREATE_FILE_MSG.format( path=log_file_params['file_path'] ) + "\n"
        # print( f"{captured.out=}" )
    finally:
        if log_file is not None:
            log_file.close()

def test_create_log_dry_run(
        log_file_params,
        capsys,
):
    params = log_file_params
    params['dry_run'] = True
    log_file = utils.create_file(
            **params
    )
    assert log_file is None
    assert not params['file_path'].exists()
    captured=capsys.readouterr()
    assert captured.out == constants.CREATE_FILE_MSG.format( path=params['file_path'] ) + "\n"
