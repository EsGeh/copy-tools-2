from copy_tools import _constants as constants

import pytest

from pathlib import Path
import filecmp
import re
import io
import os


def create_file_tree( path ):
    path.mkdir( parents=True )
    (path / "a").touch()
    (path / "b").touch()
    (path / "c").mkdir()
    (path / "c/c1").touch()
    (path / "c/c2").touch()
    (path / "d").mkdir()
    (path / "d/a").touch()

@pytest.fixture
def copy_params(tmp_path):
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = False,
            log_file = tmp_path / "config" / "log",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

@pytest.fixture
def backup_params(tmp_path):
    return dict(
            src = tmp_path / "src",
            dst = tmp_path / "dst",
            print_opts = False,
            dry_run = False,
            config_dir = tmp_path / "config",
            init_config = False,
            exclude = [],
            log_user = None,
            log_group = None,
    )

########################
# utils:
########################

def check_dirs_equal(
        src,
        dst,
        src_exclude=None,
):
    src_exclude = src_exclude or []
    src_files = dir_to_file_list( src )
    dst_files = dir_to_file_list( dst )
    excluded = []
    for pattern in src_exclude:
        if pattern.startswith( "/" ):
            path = Path(pattern).relative_to( "/" )
            excluded.append( path )
        else:
            for path in src_files:
                if pattern in str(path):
                    excluded.append( path )
    for excl in excluded:
        src_files.remove( excl )
    return dst_files, src_files

def dir_to_file_list(
        path
):
    file_list = []
    for root, dirs, files in os.walk( path ):
        root_path = Path(root).relative_to( path )
        # file_list.append( root_path )
        for x in files:
            file_list.append(
                    root_path / x
            )
    return file_list

def check_cmd_output(
        params,
        captured,
):
    # test output:
    output_lines = captured.out.split( "\n" )
    for format_str in [
            constants.START_TIME_MSG,
            constants.COPY_MSG,
            constants.RUN_CND_MSG,
            constants.RSYNC_OUTPUT_START_MSG,
            constants.RSYNC_OUTPUT_END_MSG,
    ]:
        assert_output_lines_contain_format_str(
                format_str,
                output_lines,
        )
    check_prints_opts(params, captured)
    stderr = captured.err
    assert stderr == ''

def assert_output_lines_contain_format_str(
        format_string,
        output_lines,
):
    format_string_lines = format_string.split("\n")
    assert is_subseq_ext(
            format_string_lines,
            output_lines,
            cmp_func = matches_format_str,
        ) is not None, f"expected lines {format_string_lines} not found in output"

def check_prints_opts(
        params,
        captured
):
    str_buf = io.StringIO()
    constants.print_opts(
            params,
            lambda msg: print(msg, file=str_buf)
    )
    expected_printed_opts = str_buf.getvalue()
    out = captured.out
    if params.get( 'print_opts', False ):
        assert expected_printed_opts in captured.out

def matches_format_str(
        instance,
        format_str,
):
    stripped_format_str = re.sub(
            r"\{.*\}",
            "",
            format_str,
    )
    return is_subseq( stripped_format_str, instance, )

def is_subseq(x, y):
    it = iter(y)
    return all(any(c == ch for c in it) for ch in x)

def is_subseq_ext(
        search_seq,
        seq,
        cmp_func=lambda x, y: x == y,
):
    expected_it = iter(search_seq)
    for index, token in enumerate(seq):
        expected = next(expected_it, None)
        # print( f"{index=}, {token=}, {expected=}" )
        if expected is None:
            return index - len(search_seq)
        # print( f"{cmp_func(expected,seq)=}" )
        if not cmp_func(expected,token):
            # reset iterator
            expected_it = iter(search_seq)
            continue
    expected = next(expected_it, None)
    if expected is None:
        return index - len(search_seq) + 1
    return None
