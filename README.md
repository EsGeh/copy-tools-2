# copy-tools-2 - Backup Tools based on rsync

Command line scripts to copy/synchronise files.

REMARK:
This package replaces and deprecates [copy-tools](https://github.com/EsGeh/copy-tools).
It is basically a rewrite of [copy-tools](https://github.com/EsGeh/copy-tools) in Python, which was based on [fish](https://fishshell.com/) and didn't turn out to be maintainable.

## Features

General:

- Sensible default settings
- Logging
- Automated tests

For backups:

- reasonably named subdirs and logs
- Interrupted backups are continued
- Save space on destination using hardlinks (incremental backups)

## Examples

WARNING: Always append `--dry-run` option and study the output before running any of these commands. Use at your own risk!

- Create a backup of your HOME directory in `/backups/home/`:

		$ ct-backup --print-opts /home/some-user /backups/home

- Synchronise your media driectory to `/some/dir/`:

		$ ct-copy --print-opts /home/some-user/media /some/dir

	WARNING: This operation is destructive! Files, which are not found in the source directory will be deleted in the destination.

## Usage

	$ ct-backup --help
	$ ct-copy --help

## Supported Operation Systems

- Linux

Might work on other OSes if all dependent tools are provided. So far only tested on Linux.

## Dependencies

- [rsync](https://rsync.samba.org/)
- [Python](https://www.python.org/)

## Install and test locally

	$ python -m venv venv
	$ ./venv/bin/activate
	(venv)$ pip install -e .[dev]

run tests:

	$ pytest

check test coverage:

	$ pytest --cov=src tests

## Create Python Distribution Package

	$ python -m venv venv
	$ ./venv/bin/activate
	(venv)$ pip install build
	(venv)$ python -m build

## Create Package for the arch Linux Distribution

	$ mkdir arch && cp PKGBUILD arch/ && cd arch/
	(arch/)$ makepkg -fsc

To install the package:

	(arch/)$ makepkg -i

## Reference

The following sources were used as inspiration or reference:

- [sysrsync](https://github.com/gchamon/sysrsync): rsync wrapper for Python 3
- [setuptools](https://setuptools.pypa.io/): Create distributable python packages
- [pytest](https://pytest.org/): Elegant test framework for python
- [pypi2pkgbuild](https://pypi.org/project/pypi2pkgbuildo): Create arch packages from distributable python packages
- [Arch - Python package guidelines](https://wiki.archlinux.org/title/Python_package_guidelines)
