from copy_tools import _constants as constants

import subprocess
from pathlib import Path
from functools import partial
import shutil
from typing import (
        List,
        Optional,
        Iterable,
)
import shlex
import os


class StopScriptException( Exception ):
    pass

def cmd_line_from_argv(
        argv
) -> str:
    return " ".join(map(
        shlex.quote,
        argv,
    ))

def create_dir(
        path,
        dry_run,
        print_func,
        user=None,
        group=None,
):
    if path.exists() and path.is_dir():
        return
    new_dirs = _mkdir_list_non_existent_subdirs(
        path,
    )
    print_func(
            constants.CREATE_FILE_MSG.format( path = path )
    )
    if not dry_run:
        path.mkdir(
                exist_ok=True,
                parents=True,
        )
    if user is not None or group is not None:
        for new_dir in new_dirs:
            print_func(constants.OWN_FILE.format(
                path = str(new_dir),
                user=user,
                group=group,
            ) )
            if not dry_run:
                shutil.chown(
                        new_dir,
                        user=user,
                        group=group,
                )

def create_file(
        file_path,
        dry_run,
        user,
        group,
):
    print(constants.CREATE_FILE_MSG.format( path = file_path))
    file = None
    if not dry_run:
        file = open(
                file_path,
                'a',
                encoding="utf-8",
        )
    if user is not None or group is not None:
        print(constants.OWN_FILE.format(
            path = str(file_path),
            user=user,
            group=group,
        ))
        if not dry_run:
            shutil.chown(
                    str(file_path),
                    user=user,
                    group=group,
            )
    return file

def run_process(
        command,
        print_func,
):
    process = subprocess.Popen(
            command,
            shell=False,
            stdout = subprocess.PIPE,
            stderr = subprocess.STDOUT,
    )
    print_func( constants.RSYNC_OUTPUT_START_MSG )
    while process.poll() is None:
        line = process.stdout.readline()
        print_func( line.decode(), end='' )
    print_func( constants.RSYNC_OUTPUT_END_MSG )
    return process.returncode

def copy(
        src,
        dst,
        dry_run,
        exclude: list[Path] = None,
        rsync_options: List[str] = None,
        print_func = print,
):
    """copy file or dir from 'src' to 'dst'"""
    rsync_options=rsync_options or []
    print_func(
            constants.COPY_MSG.format(
                src=src,
                dst=dst,
            )
    )
    options = constants.RSYNC_OPTIONS_DEFAULT[::]
    if dry_run:
        options.append(
                "--dry-run",
        )
    options.extend( rsync_options )
    rsync_command = _get_rsync_command(
            source = str(src),
            destination = str(dst),
            exclusions = exclude,
            options = options,
    )
    rsync_string = ' '.join(rsync_command)
    print_func( constants.RUN_CND_MSG.format( command = rsync_string ) )

    ret_code = run_process(
            command=rsync_command,
            print_func = print_func,
    )
    if ret_code != 0:
        raise StopScriptException( constants.ERROR_MSG.format(
            err = f"rsync failed with return code '{ret_code}'"
        ) )

def copy_simple(
        src,
        dst,
        dry_run,
        print_func = print,
):
    """copy file or dir from 'src' to 'dst'"""
    print_func(
            constants.COPY_MSG.format(
                src=src,
                dst=dst,
            )
    )
    options = constants.RSYNC_OPTIONS_DEFAULT[::]
    rsync_command = _get_rsync_command(
            source = str(src),
            destination = str(dst),
            options = options,
            sync_source_contents=False,
    )
    rsync_string = ' '.join(rsync_command)
    print_func( constants.RUN_CND_MSG.format( command = rsync_string ) )

    if dry_run:
        return
    process = subprocess.run(
            rsync_command,
            shell=False,
    )
    ret_code = process.returncode
    if ret_code != 0:
        raise StopScriptException( constants.ERROR_MSG.format(
            err = f"rsync failed with return code '{ret_code}'"
        ) )

#######################
# utils:
#######################

def _mkdir_list_non_existent_subdirs(
        path: Path,
):
    new_dirs = []
    current_dir = path
    while not current_dir.exists():
        new_dirs.append( current_dir )
        current_dir = current_dir.parent
    new_dirs.reverse()
    return new_dirs

def _get_rsync_command(
        source: str,
        destination: str,
        exclusions: Optional[Iterable[str]] = None,
        sync_source_contents: bool = True,
        options: Optional[Iterable[str]] = None,
) -> List[str]:

    # override sync_source_contents if local source is a file
    if os.path.isfile(source):
        sync_source_contents = False

    source, destination = sanitize_trailing_slash(
            source,
            destination,
            sync_source_contents
    )

    exclusions_options = (get_exclusions(exclusions)
                          if exclusions
                          else [])

    if options is None:
        options = []

    return [
            'rsync',
            *options,
            *exclusions_options,
            source,
            destination,
    ]

def sanitize_trailing_slash(source_dir, target_dir, sync_sourcedir_contents=True):
    # type: (str, str, bool) -> Tuple[str, str]
    target_dir = strip_trailing_slash(target_dir)

    if sync_sourcedir_contents is True:
        source_dir = add_trailing_slash(source_dir)
    else:
        source_dir = strip_trailing_slash(source_dir)

    return source_dir, target_dir


def strip_trailing_slash(directory: str) -> str:
    return (directory[:-1]
            if directory.endswith('/')
            else directory)


def add_trailing_slash(directory: str) -> str:
    return (directory
            if directory.endswith('/')
            else f'{directory}/')

def get_exclusions(excludes: Iterable[str]) -> Iterable[str]:
    options = []
    for excl in excludes:
        options.extend(
            ['--exclude', excl]
        )
    return options
