from copy_tools import _utils as utils
from copy_tools import _constants as constants

import sys
import argparse
from pathlib import Path
import time
from contextlib import contextmanager
from importlib import metadata

COMMAND_NAME = next(filter(
        lambda entry: entry.value.startswith( __name__ ),
        metadata.entry_points( group="console_scripts" ),
)).name

LOG_DIR_DEF=Path.home() / ".config" / "copy-tools-2" / COMMAND_NAME / "log"


@contextmanager
def open_log_file(
        log_file_path,
        dry_run,
        log_user,
        log_group,
):
    try:
        file = utils.create_file(
            log_file_path,
            dry_run,
            log_user,
            log_group,
        )
        yield file
    finally:
        if file is not None:
            file.close()

def run(
        now,
        **kwargs,
) -> int:
    """copy one file or dir to another"""
    kwargs['log_file'] = (
            kwargs['log_file'] or
            LOG_DIR_DEF / constants.format_time(now)
    )
    try:
        log_file_path=kwargs['log_file']
        log_file_path=log_file_path.resolve()
        log_file_dir=log_file_path.parent
        create_log_dir(
                log_file_dir,
                kwargs['init_config'],
                kwargs['dry_run'],
                kwargs['log_user'],
                kwargs['log_group'],
                print_func=print,
        )
        # INIT LOG FILE:
        with open_log_file(
                log_file_path,
                kwargs['dry_run'],
                kwargs['log_user'],
                kwargs['log_group'],
        ) as log_file:
            def output(msg, *args, **kwargs):
                print(msg, *args, **kwargs)
                if 'file' in kwargs:
                    del kwargs['file']
                if log_file is not None:
                    print(msg, file=log_file, *args, **kwargs)
            # PRINT HEADER:
            output( constants.HEADING_MSG.format(
                command_name = COMMAND_NAME,
                package_name = constants.PACKAGE_NAME,
                package_version = constants.PACKAGE_VERSION,
            ) )
            output(constants.COMMAND_MSG.format(
                command=utils.cmd_line_from_argv(
                    sys.argv
                )
            ))
            output(
                    constants.START_TIME_MSG.format(
                        time=constants.format_time(now),
                    )
            )
            if kwargs['print_opts']:
                constants.print_opts(
                        kwargs,
                        print_func = output,
                )
            src = kwargs['src']
            dst = kwargs['dst']
            utils.copy(
                    src,
                    dst,
                    dry_run = kwargs['dry_run'],
                    exclude = kwargs['exclude'],
                    print_func = output,
            )
    except utils.StopScriptException as err:
        print(
                constants.ERROR_MSG.format(err=err),
                file=sys.stderr,
        )
        return 1
    return 0

def create_log_dir(
        path,
        init_config,
        dry_run,
        log_user,
        log_group,
        print_func
):
    if path.exists() and path.is_dir():
        return
    if not init_config:
        raise utils.StopScriptException(
                f"'{path}' does not exist"
        )
    utils.create_dir(
            path,
            dry_run,
            print_func=print_func,
            user=log_user,
            group=log_group,
    )

def parse_args( argv=None ):
    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
            "src",
            metavar="SRC",
            type=Path,
    )
    parser.add_argument(
            "dst",
            metavar="DST",
            type=Path,
    )
    parser.add_argument(
            "--exclude",
            dest="exclude",
            default=[],
            action="append",
            metavar="EXCLUDE",
            help="exclude paths matching this pattern from copying. Semantics are inherited from '--exclude' option of rsync, ie. patterns that start with '/' are \"anchored\" at the SRC",
    )
    parser.add_argument(
            "--init-config",
            dest="init_config",
            action="store_true",
            help="create log dir if not existing",
    )
    parser.add_argument(
            "--log-file",
            dest="log_file",
            type=Path,
            help=f"log file path (default: '{LOG_DIR_DEF}/$DATE')",
    )
    parser.add_argument(
            "-p", "--print-opts",
            dest="print_opts",
            action='store_true',
            help=f"print all command line options",
    )
    parser.add_argument(
            "-n", "--dry-run",
            dest="dry_run",
            action='store_true',
            help=f"dont change any files on disk. All changes are logged but not executed.",
    )
    parser.add_argument(
            "-u", "--log-user",
            dest="log_user",
            metavar="LOG_USER",
            help="create log file (and directories) as different user",
    )
    parser.add_argument(
            "-g", "--log-group",
            dest="log_group",
            metavar="LOG_GROUP",
            help="create log file (and directories) as different group",
    )
    cmd_args = parser.parse_args( argv )
    return vars(cmd_args)

def main():
    cmd_args = parse_args()
    now = time.gmtime()
    cmd_args['now'] = now
    run(
            **cmd_args,
    )
