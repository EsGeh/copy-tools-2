from copy_tools import _utils as utils
from copy_tools import _constants as constants

import sys
import argparse
from pathlib import Path
import time
from typing import (
        Optional,
        Callable,
)
from dataclasses import dataclass
from importlib import metadata

COMMAND_NAME = next(filter(
        lambda entry: entry.value.startswith( __name__ ),
        metadata.entry_points( group="console_scripts" ),
)).name

CONFIG_DIR_DEF=Path.home() / ".config" / "copy-tools-2" / COMMAND_NAME


@dataclass
class VarDict:
    now: int
    kwargs: dict
    config_dir: Path
    log_dir: Path
    init_config: bool
    log_file_path: Path
    last_log_path: Path
    current_log_link: Path
    src: Path
    dst: Path
    # file structure on dst:
    dst_backup: Path
    dst_log_dir: Path
    dst_last_backup_link: Path
    print_func: Callable

class DebugBreakException( Exception ):
    pass


def run(
        now,
        emulate_interrupt = False,
        **kwargs,
) -> int:
    """copy one file or dir to another"""
    var_dict = get_var_dict(
            kwargs,
            now,
    )
    log_file = None
    def output(msg, *args, **kwargs):
        print(msg, *args, **kwargs)
        if 'file' in kwargs:
            del kwargs['file']
        if log_file is not None:
            print(msg, file=log_file, *args, **kwargs)
    var_dict.print_func = output
    try:
        init_config_dir(
                var_dict,
        )
        if var_dict.current_log_link.exists():
            var_dict.log_file_path = var_dict.log_dir / var_dict.current_log_link.readlink()
            var_dict.dst_backup = var_dict.dst_backup.parent / var_dict.log_file_path.stem
        # INIT LOG FILE:
        log_file = utils.create_file(
                var_dict.log_file_path,
                kwargs['dry_run'],
                kwargs['log_user'],
                kwargs['log_group'],
        )
        # PRINT HEADER:
        output( constants.HEADING_MSG.format(
            command_name = COMMAND_NAME,
            package_name = constants.PACKAGE_NAME,
            package_version = constants.PACKAGE_VERSION,
        ) )
        output(constants.COMMAND_MSG.format(
            command=utils.cmd_line_from_argv(
                sys.argv
            )
        ))
        output(
                constants.START_TIME_MSG.format(
                    time=constants.format_time(var_dict.now),
                )
        )
        if kwargs['print_opts']:
            constants.print_opts(
                    kwargs,
                    print_func = output,
            )
        if var_dict.current_log_link.exists():
            var_dict.print_func( constants.EXISTING_LOG_MSG.format( path = var_dict.log_file_path ) )
        else:
            create_symlink(
                    var_dict,
                    link_path=var_dict.current_log_link,
                    link_dst=Path(var_dict.log_file_path.name),
            )

        sanity_check( var_dict )
        # create dirs on DST:
        for path in [
                var_dict.dst_backup.parent,
                var_dict.dst_log_dir,
        ]:
            if not (path.exists() and path.is_dir()):
                utils.create_dir(
                        path,
                        kwargs['dry_run'],
                        print_func=output,
                )
        last_backup = find_last_backup( var_dict )
        rsync_options = []
        exclude = kwargs['exclude'][::]
        # if config dir is in src:
        # we shall exclude it and inform the user:
        if var_dict.config_dir.is_relative_to( var_dict.src  ):
            output(constants.EXCLUDE_CONFIG_MSG.format(
                path=var_dict.config_dir
            ))
            exclude.append(
                    "/" +
                    str(
                        var_dict.config_dir.relative_to( var_dict.src )
                    )
            )
        if last_backup is not None:
            output(
                    constants.FOUND_PREVIOUS_BACKUP_MSG.format( path=last_backup )
            )
            last_backup = last_backup.relative_to(
                    var_dict.dst_backup.parent
            )
            rsync_options=[
                    f"--link-dest=../{last_backup}",
            ]
        # print( f"{rsync_options=}" )
        utils.copy(
                var_dict.src,
                var_dict.dst_backup,
                dry_run = kwargs['dry_run'],
                exclude = exclude,
                rsync_options = rsync_options,
                print_func = output,
        )
        # for testing: emulate interruption:
        if emulate_interrupt:
            raise DebugBreakException( "Interrupted!" )
        utils.copy_simple(
                var_dict.log_file_path,
                var_dict.dst_log_dir / var_dict.log_file_path.name,
                dry_run = kwargs['dry_run'],
                print_func = output,
        )
        if var_dict.dst_last_backup_link.exists():
            unlink(
                    var_dict,
                    var_dict.dst_last_backup_link,
            )
        create_symlink(
                var_dict,
                var_dict.dst_last_backup_link,
                link_dst = var_dict.dst_backup,
        )
        if var_dict.last_log_path.exists():
            unlink(
                    var_dict,
                    var_dict.last_log_path,
            )
        create_symlink(
                var_dict,
                var_dict.last_log_path,
                link_dst = var_dict.log_file_path,
        )
        unlink(
                var_dict,
                var_dict.current_log_link,
        )
        # if config dir is in src:
        # it was excluded and
        # shall only be copied
        # after closing the log file:
        if var_dict.config_dir.is_relative_to( var_dict.src  ):
            log_file.close()
            log_file = None
            dest_log_path = (
                    var_dict.dst_backup
                    / var_dict.config_dir.relative_to( var_dict.src  )
            ).parent
            utils.copy_simple(
                    var_dict.config_dir,
                    dest_log_path,
                    dry_run = kwargs['dry_run'],
                    print_func = output,
            )
    except utils.StopScriptException as err:
        output(
                constants.ERROR_MSG.format(err=err),
                file=sys.stderr,
        )
        return 1
    except DebugBreakException:
        return None
    finally:
        if log_file is not None:
            log_file.close()
    return 0

def get_var_dict(
        kwargs,
        now,
):
    config_dir = (
            kwargs['config_dir']
            or CONFIG_DIR_DEF
    ).resolve()
    log_dir = config_dir / "log"
    current_log_link = log_dir / "current.log"
    src = kwargs['src'].resolve()
    dst = kwargs['dst'].resolve()
    return VarDict(
            now = now,
            kwargs = kwargs,
            config_dir = config_dir,
            log_dir = log_dir,
            init_config=kwargs['init_config'],
            log_file_path = log_dir / f"{constants.format_time(now)}.log",
            last_log_path = config_dir / f"last.log",
            current_log_link = current_log_link,
            src=src,
            dst=dst,
            # file structure on dst:
            dst_backup = dst / "backup" / constants.format_time(now),
            dst_log_dir = dst / "log",
            dst_last_backup_link = dst / "last",
            print_func=print,
    )

def init_config_dir(
        var_dict,
):
    if not var_dict.init_config:
        for path in [
                var_dict.config_dir,
                var_dict.log_dir,
        ]:
            if not (path.exists() and path.is_dir()):
                raise utils.StopScriptException(
                        f"'{path}' does not exist"
                )
    if var_dict.init_config:
        for path in [
                var_dict.config_dir,
                var_dict.log_dir,
        ]:
            if not (path.exists() and path.is_dir()):
                utils.create_dir(
                        path,
                        var_dict.kwargs['dry_run'],
                        print_func=print,
                        user=var_dict.kwargs['log_user'],
                        group=var_dict.kwargs['log_group'],
                )

def sanity_check(
        var_dict,
):
    # first try if src exists:
    if not var_dict.src.exists():
        raise utils.StopScriptException(
                constants.FILE_NOT_EXISTING_MSG.format( path = var_dict.src )
        )

def find_last_backup(
    var_dict,
) -> Optional[Path]:
    if var_dict.dst_last_backup_link.exists():
        return (
            var_dict.dst_last_backup_link.parent
            / var_dict.dst_last_backup_link.readlink()
        )
    return None

def create_symlink(
        var_dict,
        link_path,
        link_dst,
):
    if link_dst.is_absolute():
        link_dst = link_dst.relative_to(
                link_path.parent
        )
    var_dict.print_func( constants.CREATE_SYMLINK_MSG.format(
        path=link_path,
        dst=link_dst,
    ))
    if not var_dict.kwargs['dry_run']:
        link_path.symlink_to( link_dst )

def unlink(
        var_dict,
        link_path,
):
    var_dict.print_func( constants.RM_SYMLINK_MSG.format(
        path=link_path,
    ))
    if not var_dict.kwargs['dry_run']:
        link_path.unlink()

def parse_args( argv=None ):
    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
            "src",
            metavar="SRC",
            type=Path,
    )
    parser.add_argument(
            "dst",
            metavar="DST",
            type=Path,
    )
    parser.add_argument(
            "--exclude",
            dest="exclude",
            default=[],
            action="append",
            metavar="EXCLUDE",
            help="exclude paths matching this pattern from copying. Semantics are inherited from '--exclude' option of rsync, ie. patterns that start with '/' are \"anchored\" at the SRC",
    )
    parser.add_argument(
            "--config-dir",
            dest="config_dir",
            type=Path,
            help=f"config dir path (default: '{CONFIG_DIR_DEF}",
    )
    parser.add_argument(
            "--init-config",
            dest="init_config",
            action="store_true",
            help="create config dir if not existing",
    )
    parser.add_argument(
            "-p", "--print-opts",
            dest="print_opts",
            action='store_true',
            help=f"print all command line options",
    )
    parser.add_argument(
            "-n", "--dry-run",
            dest="dry_run",
            action='store_true',
            # default=False,
            help=f"dont change any files on disk. All changes are logged but not executed.",
    )
    parser.add_argument(
            "-u", "--log-user",
            dest="log_user",
            metavar="LOG_USER",
            help="create log file (and directories) as different user",
    )
    parser.add_argument(
            "-g", "--log-group",
            dest="log_group",
            metavar="LOG_GROUP",
            help="create log file (and directories) as different group",
    )
    cmd_args = parser.parse_args( argv )
    return vars(cmd_args)

def main():
    now = time.gmtime()
    cmd_args = parse_args()
    cmd_args['now'] = now
    run(
            **cmd_args,
    )
