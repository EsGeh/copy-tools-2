from pathlib import Path
import time as time_module
from importlib import metadata


PACKAGE_NAME = "copy-tools-2"
PACKAGE_VERSION = metadata.version( PACKAGE_NAME )

RSYNC_OPTIONS_DEFAULT = [
        "-a",
        "-A",
        "-X",
        "-H",
        "--itemize-changes",
        "--update",
        "--delete",
]

# output templates:
SEP_STR="------------------------"
CREATE_FILE_MSG = "create '{path}'"
RM_FILE_MSG = "rm '{path}'"
CREATE_SYMLINK_MSG = "symlink '{path}' -> '{dst}'"
RM_SYMLINK_MSG = "unlink '{path}'"
COPY_MSG = "copy '{src}' -> '{dst}'"
RUN_CND_MSG = "running '{command}'"
RSYNC_OUTPUT_START_MSG = f"{SEP_STR}\nRSYNC OUTPUT START:"
RSYNC_OUTPUT_END_MSG = "RSYNC OUTPUT END:"
ERROR_MSG = "ERROR: {err}"
FILE_NOT_EXISTING_MSG = "'{path}' does not exist"
OWN_FILE = "set owner '{user}': '{path}'"
START_TIME_MSG = "time: {time}"


HEADING_MSG = (
    f"***************************************\n"
    f"* COMMAND: {{command_name}}\n"
    f"* PACKAGE: {{package_name}} v. {{package_version}}\n"
    f"***************************************"
)
COMMAND_MSG = "command:\n $ {command}"
EXISTING_LOG_MSG = "last backup wasn't finished. continuing '{path}'..."
FOUND_PREVIOUS_BACKUP_MSG = "found previous backup '{path}'"
EXCLUDE_CONFIG_MSG = (
        "config directory is inside the src dir and will be excluded."
        " It will be silently copied, after the log has been closed '{path}'"
)

def format_time( time ):
    return time_module.strftime( "%Y-%m-%d_%H:%M:%S", time)

def print_opts(
        kwargs,
        print_func = print,
):
    """print dictionary in a user readable format"""
    print_func(SEP_STR)
    for key, value in kwargs.items():
        value_str = val_string(value)
        print_func(f"{key}: {value_str}")
    print_func(SEP_STR)

def val_string(
        value
):
    if isinstance(value, list):
        return ', '.join(map(
            lambda x: f"'{x}'",
            value,
        ))
    if isinstance(value, Path):
        return f"'{value}'"
    if isinstance(value, str):
        return f"'{value}'"
    return value
